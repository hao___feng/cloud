const xmlParser = require("koa-xml-body");

module.exports = () => {
  return async (ctx, next) => {
    await xmlParser({
      xmlOptions: {
        trim: true,
        explicitArray: false
      },
      onerror: (err, ctx) => {
        ctx.throw(err.status, err.message);
      }
    })(ctx, async () => {
      if (ctx.request.body && ctx.request.body.xml) {
        ctx.xml = ctx.request.body.xml;
      }
    });

    await next();
  };
};
