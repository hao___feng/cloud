const router = require("koa-router")();

// 数据解密
router.get("/class/:className/verify/:authName", async (ctx, next) => {
  const { className, authName } = ctx.params;

  if (!ctx.auth[authName]) {
    throw new Error(`Class ${className} AuthName ${authName} Not Found`);
  }

  ctx.success(ctx.auth[authName], "success");
});

module.exports = router;
