/** 数据表**/
const router = require("koa-router")();
/**
 * api {get} /home/app/auth/class/table 数据表信息
 *
 */
router.get("/class/table", async (ctx, nex) => {
  const user = ctx.user;
  const baas = ctx.baas;
  const table = await BaaS.Models.class_table
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: ["class", "auth"]
    });
  ctx.success(table, "数据表信息");
});
/**
 * api {get} /home/app/auth/info/class/table 数据表额外信息
 *
 */
router.get("/info/class/table", async (ctx, nex) => {
  const user = ctx.user;
  const baas = ctx.baas;

  // 分组
  const group = await BaaS.Models.class
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: []
    });
  const table = await ctx.table(baas.database); // 数据表
  // 秘钥
  const auth = await BaaS.Models.auth
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: []
    });

  const data = {
    group: group,
    table: table,
    auth: auth
  };

  ctx.success(data, "数据表额外信息");
});
/**
 * api {get} /home/app/auth/class/table/:id 获取单个分组
 *
 * apiParam {Number} id
 *
 */
router.get("/class/table/:id", async (ctx, nex) => {
  const baas = ctx.baas;
  const { id } = ctx.params;

  const table = await BaaS.Models.class_table
    .query(qb => {
      qb.where("id", "=", id);
      qb.where("baas_id", "=", baas.id);
    })
    .fetch({
      withRelated: ["class", "auth"]
    });

  ctx.success(table, "单个分组信息");
});
/**
 * api {post} /home/app/auth/add/class/table 新增修改权限
 * 
 * apiParam {
		id: id,
		class_id: class_id,
		table: order, //表名
		fetch: 1,
		add: 1,
		update: 1,
		del: 1,
		auth_id: auth_id
	} 
 * 
 */
router.post("/add/class/table", async (ctx, nex) => {
  const baas = ctx.baas;

  const classId = ctx.post.class_id;
  const authId = ctx.post.auth_id ? ctx.post.auth_id : 0;
  const { id, table, fetch, add, update, del } = ctx.post;

  const classTable = await BaaS.Models.class_table
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.where("class_id", "=", classId);
      qb.where("table", "=", table);
    })
    .fetch();
  if (classTable && classTable.id != id) {
    ctx.error("", `此分组下已存在数据表${table}`);
    return;
  }

  const result = await BaaS.Models.class_table
    .forge({
      id: id,
      baas_id: baas.id,
      class_id: classId,
      table: table,
      fetch: fetch,
      add: add,
      update: update,
      delete: del,
      auth_id: authId
    })
    .save();

  // 删除redis缓存
  await BaaS.redis.delAll(
    `baas:*:appid:${baas.appid}:appkey:${baas.appkey}:*:classTable`
  );

  ctx.success(result, "保存成功");
});
/**
 * api {get} /home/app/auth/del/class/table/:id 删除权限
 *
 * apiParam {Number} id
 *
 */
router.get("/del/class/table/:id", async (ctx, nex) => {
  const baas = ctx.baas;
  const { id } = ctx.params;

  const classTable = await BaaS.Models.class_table
    .query(qb => {
      qb.where("id", "=", id);
      qb.where("baas_id", "=", baas.id);
    })
    .fetch();
  if (!classTable) {
    ctx.error("", "参数错误");
    return;
  }

  const result = await BaaS.Models.class_table
    .forge({
      id: id
    })
    .destroy();

  // 删除redis缓存
  await BaaS.redis.delAll(
    `baas:*:appid:${baas.appid}:appkey:${baas.appkey}:*:classTable`
  );

  ctx.success(result, "删除权限");
});

module.exports = router;
