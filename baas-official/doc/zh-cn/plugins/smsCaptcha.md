# 短信验证码

##### 发送短信验证码

```js
const phone = '18538253627';
const code = '123456';
const smsResult = await module.sendSmsCode(phone, code);
```

返回结果

```js
// 发送成功返回示例
{
    "result": {
        "err_code": "0",
        "model": "179910513155111476^0",
        "msg": "OK",
        "success": true
    },
    "request_id": "13f2kqfarz4ic"
}
// 短信量不足返回示例
{
    "data": "Error : Sorry, short message",
    "code": 0
}
```