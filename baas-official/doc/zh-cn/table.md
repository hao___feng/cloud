# 数据表

### 模型管理

1.支持设置UUID，当开启时，数据表的主键将会自动生成24位随机ID

2.支持设置唯一字段，当开启时，插入数据表的数据将是唯一的

3.支持设置隐藏字段，当开启时，数据表查询出来的数据，将会隐藏此字段

### 关联模型

1.hasOne

2.hasMany

3.belongsTo

4.belongsToMany

5.morphTo

6.morphOne

7.morphMany



### 增删改查

#### 查询

| 请求方式 | URL参数     | 说明     |
| ---- | --------- | ------ |
| GET  | fetch     | 查询一条数据 |
| GET  | fetchAll  | 查询多条数据 |
| GET  | fetchPage | 查询分页数据 |
|      |           |        |

示例查询

```
方式：
GET

链接 ：
https://baas.qingful.com/2.0/class/public/table/user/fetch?where=phone,1&where=password,1

说明：
查询user数据表，user表的phone字段等于1并且password字段等于1
```

#### 新增/更新

> 提示：当post传递参数id时为更新数据，当post没有传递参数id或者id=0时为新增数据

| 请求方式 | URL参数  | 说明      |
| ---- | ------ | ------- |
| POST | add    | 新增/更新数据 |
| POST | save   | 新增/更新数据 |
| POST | update | 新增/更新数据 |
|      |        |         |

示例新增

```
方式：
POST

链接：
https://baas.qingful.com/2.0/class/home/table/todos/add

参数：
{
   "content": "demo"
}

说明：
数据表todos，新增数据
```

#### 删除

> 提示：删除数据时特别注意，推荐使用软删除。

| 请求方式 | URL参数   | 说明   |
| ---- | ------- | ---- |
| GET  | del     | 删除数据 |
| GET  | delete  | 删除数据 |
| GET  | destroy | 删除数据 |
|      |         |      |

示例删除

```
方式：
GET

链接 ：
https://baas.qingful.com/2.0/class/home/table/todos/delete?where=id,1

说明：
数据表todos，删除id等于1的数据
```

### 请求参数

> 重要：url参数如果有表达式，需要url转义。比如where=id,>,1中id,>,1需要url转义
> js转义推荐使用encodeURIComponent，php转义推荐使用urlencode

> 重要：表达式支持>, <, =, >=, <=, !=, like（模糊查询）

| 方法                | 说明                        | 示例1                                 | 示例2                       |
| ----------------- | ------------------------- | ----------------------------------- | ------------------------- |
| where             | 表达式查询                     | where=id,1（查询id等于1）                 | where=id,>,1              |
| orWhere           | 表达式查询                     | orWhere=id,1                        | orWhere=id,=,1            |
| whereNot          | 表达式查询                     | whereNot=id,1                       | whereNot=id,<,1           |
| orWhereNot        | 表达式查询                     | orWhereNot=id,1                     | orWhereNot=id,<,1         |
| whereIn           | 区间查询                      | whereIn=id,1,2（查询id等于1或者2）          |                           |
| orWhereIn         | 区间查询                      | orWhereIn=id,1,2                    |                           |
| whereNotIn        | 区间查询                      | whereNotIn=id,1,2                   |                           |
| orWhereNotIn      | 区间查询                      | orWhereNotIn=id,1,2                 |                           |
| whereNull         | 基础查询                      | whereNull=id（查询id等于null）            |                           |
| orWhereNull       | 基础查询                      | orWhereNull=id                      |                           |
| whereNotNull      | 基础查询                      | whereNotNull=id                     |                           |
| orWhereNotNull    | 基础查询                      | orWhereNotNull=id                   |                           |
| whereBetween      | 区间查询                      | whereBetween=id,1,10（查询id在1到10之间的数据 |                           |
| whereNotBetween   | 区间查询                      | whereNotBetween=id,1,10             |                           |
| orWhereBetween    | 区间查询                      | orWhereBetween=id,1,10              |                           |
| orWhereNotBetween | 区间查询                      | orWhereNotBetween=id,1,10           |                           |
| limit             | 限制条件查询，仅查询几条，默认是20条       | limit=20                            |                           |
| offset            | 偏移查询，偏移几条                 | offset=12                           |                           |
| orderBy           | 排序查询，默认id倒序               | orderBy=id,desc（id倒序）               | orderBy=id,asc（id正序）      |
| column            | 字段查询，默认查询出全部字段            | column=id                           |                           |
| count             | 统计查询，查询满足条件的数据是多少条        | count=id,name                       |                           |
| sum               | 统计查询，查询满足条件的字段的总和是多少      | sum=price                           |                           |
| min               | 统计查询，查询满足条件的字段的最小值是多少     | min=price                           |                           |
| max               | 统计查询，查询满足条件的字段的最大值是多少     | max=price                           |                           |
| avg               | 统计查询，查询满足条件的字段的平均数是多少     | avg=price                           |                           |
| page              | 分页查询，第几页（fetchPage时有效）    | page=1                              |                           |
| pageSize          | 分页查询，每页数量（fetchPage时有效）   | pageSize=20                         |                           |
| deleted           | 查询已删除的数据，默认是false         | deleted=false                       |                           |
| forceDelete       | 强制删除（已删除的数据不能查询），默认是false | forceDelete=false                   |                           |
| related           | 关联查询，支持深层次关联              | related=order,shop                  | related=order.detail,shop |
| joined            | 关联查询，支持深层次关联，同related     | joined=order,shop                   | joined=order.detail，shop  |
| join              | 内连接                       | join=order,detail.order_id,order.id |                           |
| innerJoin         | 内连接                       |                                     |                           |
| leftJoin          | 左连接                       |                                     |                           |
| leftOuterJoin     | 左外连接                      |                                     |                           |
| rightJoin         | 右连接                       |                                     |                           |
| rightOuterJoin    | 右外连接                      |                                     |                           |
| outerJoin         | 外连接                       |                                     |                           |
| fullOuterJoin     | 全外连接                      |                                     |                           |
| crossJoin         | 交叉连接                      |                                     |                           |
|                   |                           |                                     |                           |

示例查询1

```
方式：
GET

链接 ：
https://baas.qingful.com/2.0/class/public/table/user/fetch?where=phone,1&where=password,1

说明：
查询user数据表，user表的phone字段等于1并且password字段等于1
```

示例查询2

> 提示：related关联条件查询格式支持related\_[表名]\_[方法]，方法支持部分请求参数例如orWhere，whereIn。

```
方式：
GET

链接 ：
https://baas.qingful.com/2.0/class/public/table/user/fetch?related=order&where=id,1&related_order_where=id,1

说明：
查询user数据表，user表的id字段等于1，关联出order表并且order表的id字段等于1
```

示例查询3

> 提示：示例2和示例3差别在于，示例3是内连接，示例2是左连接。推荐使用示例2的查询方法，查询关联数据。示例3属于高级用法。

```
方式：
GET

链接 ：
https://baas.qingful.com/2.0/class/public/table/user/fetch?joined=order&join=order,user.id,order.user_id&where=user.id,1&where=order.id,1

说明：
查询user数据表，user表的id字段等于1，关联出order表并且order表的id字段等于1
```